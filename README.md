# Guia de Estilos Nitronews
## Instalação

```bash
$ npm install @nitrodigital/guia-de-estilos
```

[Leia mais aqui](https://docs.npmjs.com/cli/install) sobre qual flag utilizar durante a instalação. (Artigo em inglês) 

## Uso

```js
import Audrye from 'guia-de-estilos';
```

## Contribuir

Encontrou algum bug? Fale conosco através do [Gitlab Issues](https://gitlab.com/nitronews_sitema_2015/guia-de-estilos/issues).

Se você souber como resolver, envie-nos um merge request e avaliaremos seu código.

Para clonar o projeto e deixar ele pronto para uso, execute os seguintes comandos:

```bash
$ git clone https://gitlab.com/nitronews_sitema_2015/guia-de-estilos
$ cd guia-de-estilos
$ npm link
```

Navegue até o seu projeto, então execute:

```bash
$ npm link @nitrodigital/guia-de-estilos
```
